/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

(function () {
    var config = {
    apiKey: "AIzaSyAd3RHx4vD9-jm6ienYMk8w0DxdXhXbzXU",
    authDomain: "web-app-c65b6.firebaseapp.com",
    databaseURL: "https://web-app-c65b6.firebaseio.com",
    projectId: "web-app-c65b6",
    storageBucket: "web-app-c65b6.appspot.com",
    messagingSenderId: "992391151637"
  };
    firebase.initializeApp(config);

    const txtEmail = document.getElementById('email');
    const txtSenha = document.getElementById('senha');
    const btnEntrar = document.getElementById('entrar');
    
    //const btnCadastrar = document.getElementById('cadastrar');

    btnEntrar.addEventListener('click', e => {
        const email = txtEmail.value;
        const senha = txtSenha.value;
        const auth = firebase.auth();
        const ret = auth.signInWithEmailAndPassword(email, senha);
        ret.catch(e => alert(e.message));

    });

    firebase.auth().onAuthStateChanged(firebaseUser => {
        if(firebaseUser){
            if(firebaseUser.email.indexOf("@admin.com") != -1){
                window.location.replace("admin.html");  
            }
            else if(firebaseUser.email.indexOf("@inatel.br") != -1){
                     window.location.replace("funcionario.html");  
            }
        }
    })
    

}())
        ;

